import simaa_helpers

assets = [
    'SPXL',
    'SPXS',
]
shares = [
    0.8,
    0.2,
]
simaa_helpers.sim(assets, shares, freq=1)
