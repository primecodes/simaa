import simaa_helpers

assets = [
    'SPXL',
    'TMF',
    'TYD',
    'GLD',
    'DBC',
]
shares = [
    0.2,
    0.2,
    0.2,
    0.2,
    0.2,
]
simaa_helpers.sim(assets, shares)
