import simaa_helpers

assets = [
    'VT',
    'EDV',
    'LTPZ',
    'VCLT',
    'VWOB',
    'IAU',
    'BCI',
]
shares = [
    0.35,
    0.2,
    0.2,
    0.075,
    0.075,
    0.05,
    0.05,
]
simaa_helpers.sim(assets, shares)
