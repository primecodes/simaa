import simaa_helpers

assets = [
    'SPY',
    'TLT',
    'IEF',
    'GLD',
    'DBC',
]
shares = [
    0.3,
    0.4,
    0.15,
    0.075,
    0.075,
]
simaa_helpers.sim(assets, shares)
