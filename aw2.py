import simaa_helpers

assets = [
    'SPY',
    'EFA',
    'EEM',
    'EDV',
    'LTPZ',
    'LQD',
    'VWOB',
    'GLD',
    'DBC',
]
shares = [
    0.12,
    0.12,
    0.12,
    0.18,
    0.18,
    0.07,
    0.07,
    0.07,
    0.07,
]
simaa_helpers.sim(assets, shares)
