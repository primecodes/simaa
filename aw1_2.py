import simaa_helpers

assets = [
    '251350',
    '122630',
    '304660',
    '305080',
    '132030',
    '271060',
    '139310',
    '144600',
    '130680',
]
shares = [
    0.15,
    0.15,
    0.4,
    0.155,
    0.075,
    0.0175,
    0.0175,
    0.0175,
    0.0175,
]
simaa_helpers.sim(assets, shares, source='krx')
