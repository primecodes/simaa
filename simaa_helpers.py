import argparse
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import pandas as pd
from dateutil.relativedelta import relativedelta

import chart_source_helpers
import ticker_name_dict_3


def filter_df(df, from_d=None, to_d=None, dropna=False):
    if dropna:
        df = df.dropna()
    if from_d:
        df = df[df.index >= from_d]
    if to_d:
        df = df[df.index < to_d]
    return df


def debug(args, ticker, n, p):
    if args.debug:
        print(
            f"{ticker}|"
            f"{n:20,}|"
            f"{p:20,}|"
            f"{n * p:30,}|"
            f'{ticker_name_dict_3.find(ticker)["name"]}'
        )


def log(dt, bench, v1, v2):
    print(f"{dt}|{bench:30,f}|{v1:30,f}|{v2:30,f}")


def savefig(plt, fname):
    Path(fname).parent.mkdir(exist_ok=True, parents=True)
    plt.savefig(fname)


def e1(assets, schedule, bench, args):
    seed = args.seed
    graph = args.graph
    df_sim = pd.DataFrame()
    ticker_bench, df_bench = bench
    print(
        f"{ticker_bench:10}|"
        f"{1:,f}|"
        f'{ticker_name_dict_3.find(ticker_bench)["name"]}'
    )
    print()
    df_bench = filter_df(df_bench, from_d=schedule[0])
    p = df_bench["Close"].iloc[0]
    n_bench = seed // p
    df_sim = pd.DataFrame()
    df_sim["bench"] = n_bench * df_bench["Close"]
    df_sim["v1"] = 0
    df_sim["v2"] = 0
    dfs = []
    from_d = schedule[0]
    to_d = schedule[1]
    ns1 = []
    sum = 0
    for ticker, share, df in assets:
        df_temp = filter_df(df, from_d=from_d, to_d=to_d)
        v = share * seed
        p = df_temp["Close"].iloc[0]
        n = v // p
        sum += n * p
        debug(args, ticker, n, p)
        df_temp["N1"] = n
        df_temp["N2"] = n
        ns1.append(n)
        dfs.append(df_temp)
    log(schedule[0], df_sim["bench"].iloc[0], sum, sum)
    ns2 = ns1.copy()
    for y in range(1, len(schedule)):
        from_d = schedule[y]
        to_d = schedule[y + 1] if y < len(schedule) - 1 else None
        sum1 = 0
        sum2 = 0
        empty = False
        for x, (ticker, share, df) in enumerate(assets):
            df_temp = filter_df(df, from_d=from_d, to_d=to_d).copy()
            if len(df_temp) == 0:
                empty = True
                break
            p = df_temp["Close"].iloc[0]
            df_temp["N1"] = ns1[x]
            df_temp["N2"] = ns2[x]
            dfs[x] = pd.concat([dfs[x], df_temp])
            sum1 += ns1[x] * p
            sum2 += ns2[x] * p
        if not empty:
            log(
                schedule[y],
                df_sim[df_sim.index >= schedule[y]]["bench"].iloc[0],
                sum1,
                sum2,
            )
            ns2 = []
            for ticker, share, df in assets:
                df_temp = filter_df(df, from_d=from_d, to_d=to_d)
                v = share * sum2
                p = df_temp["Close"].iloc[0]
                n = v // p
                debug(args, ticker, n, p)
                ns2.append(n)
    for df in dfs:
        df_sim["v1"] += df["N1"] * df["Close"]
        df_sim["v2"] += df["N2"] * df["Close"]
    if graph is not None:
        try:
            colors = plt.rcParams["axes.prop_cycle"].by_key()["color"]
            fig, ax = plt.subplots(1, figsize=(20, 5))
            ax.plot(df_sim["bench"], color="r", label="benchmark")
            ax.plot(df_sim["v1"], color="#ccc", label="strategy (no rebalance)")
            ax.plot(df_sim["v2"], color="k", label="strategy (rebalance)")
            ax.set_xlabel("Time")
            ax.set_ylabel("Price")
            ax.yaxis.tick_right()
            ax.yaxis.set_label_position("right")
            ax.legend(loc="upper left", framealpha=0.25)
            ax.spines["top"].set_visible(False)
            ax.spines["right"].set_visible(False)
            ax.spines["bottom"].set_visible(False)
            ax.spines["left"].set_visible(False)
            plt.tight_layout()
            Path("temp").mkdir(exist_ok=True, parents=True)
            savefig(plt, f"temp/{graph}.png")
        finally:
            plt.close()


def load(ticker, interval=None, source="yf", reload=False):
    if source == "yf":
        return chart_source_helpers.load_yf(ticker, interval=interval)
    if source == "krx":
        # return chart_source_helpers.load_krx(ticker, interval=interval, reload=reload)
        return chart_source_helpers.load_cache(ticker, interval=interval)
    if source == "cache":
        return chart_source_helpers.load_cache(ticker, interval=interval)
    raise Exception(f"unknown source: {source}")


def load_assets(assets, shares, interval, source="yf", reload=False):
    for x, ticker in enumerate(assets):
        print(
            f"{ticker:10}|"
            f"{shares[x]:,f}|"
            f'{ticker_name_dict_3.find(ticker)["name"]}'
        )
    print(f'{10 * " "}|{sum(shares):,f}|')
    print()
    rs = []
    from_d_s = []
    for x, ticker in enumerate(assets):
        df, _ = load(ticker, interval=interval, source=source, reload=reload)
        rs.append((ticker, shares[x], df))
        from_d_s.append(df.index[0])
    from_d = max(from_d_s)
    return rs, from_d


def mk_schedule(from_d, freq=6):
    schedule = [from_d]
    while True:
        from_d = from_d + relativedelta(months=freq)
        if from_d.strftime("%Y-%m-%d") > datetime.now().strftime("%Y-%m-%d"):
            break
        schedule.append(from_d)
    return schedule


def argparser():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "-S",
        "--seed",
        type=float,
        default=100_000_000,
        help="seed money",
    )
    parser.add_argument(
        "-b",
        "--benchmark",
        default=None,
        help="benchmark " "(format: <ticker> or <ticker>,<interval>,<source>)",
    )
    parser.add_argument(
        "-f",
        "--freq",
        type=int,
        default=6,
        help="rebalancing frequency in months",
    )
    parser.add_argument(
        "-g",
        "--graph",
        default=None,
        help="graph file name",
    )
    parser.add_argument(
        "-d",
        "--debug",
        action="store_true",
        help="show debug",
    )
    parser.add_argument(
        "--reload",
        action="store_true",
        help="get the newest data",
    )
    return parser


def get_benchmark(source, interval, args):
    bench = args.benchmark
    reload = args.reload
    if bench is None:
        if source == "krx":
            ticker = "069500"
        elif source == "cache":
            ticker = "069500"
        elif source == "yf":
            ticker = "SPY"
        else:
            raise f"unknown source: {source}"
    else:
        cs = bench.split(",")
        if len(cs) == 1:
            ticker = cs[0]
        elif len(cs) == 3:
            ticker = cs[0]
            interval = cs[1]
            source = cs[2]
        else:
            raise f"invalid benchmark: {bench}"
    df, _ = load(ticker, interval=interval, source=source, reload=reload)
    return ticker, df


def sim(assets, shares, interval=None, source="yf", freq=0, benchmark=None):
    args = argparser().parse_args()
    if freq == 0:
        freq = args.freq
    assets, from_d = load_assets(
        assets, shares, interval, source=source, reload=args.reload
    )
    ticker_becnh, df_bench = get_benchmark(source, interval, args)
    from_d = max(from_d, df_bench.index[0])
    schedule = mk_schedule(from_d, freq=freq)
    e1(assets, schedule, (ticker_becnh, df_bench), args)


if __name__ == "__main__":
    assets = [
        "SPY",
        "TLT",
        "IEF",
        "GLD",
        "DBC",
    ]
    shares = [
        0.3,
        0.4,
        0.15,
        0.075,
        0.075,
    ]
    sim(assets, shares)
